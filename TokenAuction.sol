pragma solidity >=0.6.0 <0.8.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/solc-0.6/contracts/access/Ownable.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/solc-0.6/contracts/math/SafeMath.sol";

interface IERC271Token {
    //function balanceOf(address owner) external returns (uint256);
    function balanceOf(address owner) external view returns (uint256 balance);
    // function transfer(address to, uint256 amount) external returns (bool);
    function name() external returns (string memory);
    function owner() external returns (string memory);
    function ownerOf(uint256 tokenId) external view returns (address);
}



contract TokenAuction { 
    
    using SafeMath for uint256;
    
    IERC271Token public token;
    
      struct Auction {
        address seller;
        uint128 price;
        }
    
    constructor (       
        address _tokencontract
        ) 
        public 
        {
        token = IERC271Token(_tokencontract);
    
    }
    
    
    // any bidder can withdrawal ether at any point except for the current high bidder
        function withdraw() public {
            
    }
    
    // bidding function
    
        event Bid(address highBidder, uint256 highBid);
    
        function bid(uint256 amount) public payable {

    }
    
    //resolve / end auction function - can be called by anyone
    
    function balance() public view returns (uint256){
       uint256 _balance = token.balanceOf(0x5B38Da6a701c568545dCfcB03FcB875f56beddC4);
       return _balance;
       
    }
    
 
    
}